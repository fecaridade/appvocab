﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mono.Data.Sqlite;
using System.IO;
using System.Data;
using System.Linq;
public class AppControler : MonoBehaviour
{
    public string LinguaAtual;
    public List<Sprite> Imagem_Linguas_Ativas;
    public List<Sprite> Imagem_Linguas_NaoAtivas;
    public List<GameObject> ObjetosSessoes;
    public List<Sprite> Sprites_Frases_Expressoes;
    public List<Sprite> AssetsFraseseExpressoes;
    public Image Titulo_Frases_Expressoes;
    public string[] TraducaoPTBR;
    public string[] TraducaoEN;
    public string[] TraducaoAL;
    public string[] TraducaoES;
    public string[] TraducaoITA;
    public Button Portugues;
    public Button Ingles;
    public Button Alemao;
    public Button Espanhol;
    public Button Italiano;
    public GameObject GPortugues;
    public GameObject GIngles;
    public GameObject GAlemao;
    public GameObject GEspanhol;
    public GameObject GItaliano;
    public bool PAtivo;
    public bool InAtivo;
    public bool AlAtivo;
    public bool EsAtivo;
    public bool ItaAtivo;
    int Pindex_ativo = 0;
    int Inindex_ativo = 1;
    int Alindex_ativo = 2;
    int Esindex_ativo = 3;
    int Itaindex_ativo = 4;
    public AudioManager audioManagerInstance;
    public Text TextoSessao;
    public Button Frases_Expressoes;
    public GameObject TituloSessoes;
    public GameObject Baseprefab;
    public string SessaoAtual;
    public Vector3 inicialposition;
    public Vector3 inicialpositionSec;
    public Vector3 lasposition;
    public Vector3 offset = new Vector3(0, 0, 0);
    public bool FrasesEExpressosCriado;
    public string dbName;

    void Start()
    {
        Portugues.onClick.AddListener(selecionaPortugues);
        Ingles.onClick.AddListener(selecionaIngles);
        Alemao.onClick.AddListener(selecionaAlemao);
        Espanhol.onClick.AddListener(selecionaEspanhol);
        Italiano.onClick.AddListener(selecionaItaliano);
        Frases_Expressoes.onClick.AddListener(Escolhe_sessao_Frases_Expressoes);
        //Frases_Expressoes.onClick.AddListener(TesteSql);
        dbName = "URI=file:" + Application.dataPath + "/palavras.s3db";
        Debug.Log(Application.dataPath);
        TextoSessao = GameObject.Find("TextoSC").GetComponent<Text>();
        PAtivo = true;
        LinguaAtual = "Portugues";
        inicialposition = new Vector3(550, 1700, 0);
        inicialpositionSec = new Vector3(0, 1970, 0);
        Verifica_Ativo();
    }
    void TesteSql()
    {
        using (var connection = new SqliteConnection(dbName))
        {
            connection.Open();
            using (var command = connection.CreateCommand())
            {
                string sqlcmd = "SELECT * FROM Palavras WHERE Sessao == 'Cores'";
                command.CommandText = sqlcmd;
                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Debug.Log(reader[1]);
                    }
                }
            }
            connection.Clone();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Verifica_Ativo();
        
    }
    void Troca_Lingua()
    {
        if (LinguaAtual == "Portugues")
        {
            TextoSessao.text = "Seções";
            for(int i = 0; i<ObjetosSessoes.Count; i++)
            {
                ObjetosSessoes[i].GetComponentInChildren<Text>().text = TraducaoPTBR[i];
            }
        }
        else if (LinguaAtual == "Ingles")
        {
            TextoSessao.text = "Sections";
            for (int i = 0; i < ObjetosSessoes.Count; i++)
            {
                ObjetosSessoes[i].GetComponentInChildren<Text>().text = TraducaoEN[i];
            }
        }
        else if (LinguaAtual == "Alemao")
        {
            TextoSessao.text = "Abschnitte";
            for (int i = 0; i < ObjetosSessoes.Count; i++)
            {
                ObjetosSessoes[i].GetComponentInChildren<Text>().text = TraducaoAL[i];
            }
        }
        else if (LinguaAtual == "Espanhol")
        {
            TextoSessao.text = "Secciones";
            for (int i = 0; i < ObjetosSessoes.Count; i++)
            {
                ObjetosSessoes[i].GetComponentInChildren<Text>().text = TraducaoES[i];
            }
        }
        else if (LinguaAtual == "Italiano")
        {
            TextoSessao.text = "Sezioni";
            for (int i = 0; i < ObjetosSessoes.Count; i++)
            {
                ObjetosSessoes[i].GetComponentInChildren<Text>().text = TraducaoITA[i];
            }
        }
    }
    void Verifica_Ativo()
    {
        if (PAtivo)
        {
            GPortugues.GetComponent<Image>().sprite = Imagem_Linguas_Ativas[Pindex_ativo];
            
        }
        else
        {
            GPortugues.GetComponent<Image>().sprite = Imagem_Linguas_NaoAtivas[Pindex_ativo];
        }
        if (InAtivo)
        {
            GIngles.GetComponent<Image>().sprite = Imagem_Linguas_Ativas[Inindex_ativo];
        }
        else
        {
            GIngles.GetComponent<Image>().sprite = Imagem_Linguas_NaoAtivas[Inindex_ativo];
        }
        if (AlAtivo)
        {
            GAlemao.GetComponent<Image>().sprite = Imagem_Linguas_Ativas[Alindex_ativo];
        }
        else
        {
            GAlemao.GetComponent<Image>().sprite = Imagem_Linguas_NaoAtivas[Alindex_ativo];
        }
        if (EsAtivo)
        {
            GEspanhol.GetComponent<Image>().sprite = Imagem_Linguas_Ativas[Esindex_ativo];
        }
        else
        {
            GEspanhol.GetComponent<Image>().sprite = Imagem_Linguas_NaoAtivas[Esindex_ativo];
        }
        if (ItaAtivo)
        {
            GItaliano.GetComponent<Image>().sprite = Imagem_Linguas_Ativas[Itaindex_ativo];
        }
        else
        {
            GItaliano.GetComponent<Image>().sprite = Imagem_Linguas_NaoAtivas[Itaindex_ativo];
        }

    }
    void selecionaPortugues()
    {
        LinguaAtual = "Portugues";
        PAtivo = true;
        InAtivo = false;
        AlAtivo = false;
        EsAtivo = false;
        ItaAtivo = false;
        audioManagerInstance.Play("som_click");
        Troca_Lingua();
        AtualizaSessoes();

    }
    void selecionaIngles()
    {
        LinguaAtual = "Ingles";
        PAtivo = false;
        InAtivo = true;
        AlAtivo = false;
        EsAtivo = false;
        ItaAtivo = false;
        audioManagerInstance.Play("som_click");
        Troca_Lingua();
        AtualizaSessoes();
    }
    void selecionaAlemao()
    {
        LinguaAtual = "Alemao";
        PAtivo = false;
        InAtivo = false;
        AlAtivo = true;
        EsAtivo = false;
        ItaAtivo = false;
        audioManagerInstance.Play("som_click");
        Troca_Lingua();
        AtualizaSessoes();
    }
    void selecionaEspanhol()
    {
        LinguaAtual = "Espanhol";
        PAtivo = false;
        InAtivo = false;
        AlAtivo = false;
        EsAtivo = true;
        ItaAtivo = false;
        audioManagerInstance.Play("som_click");
        Troca_Lingua();
        AtualizaSessoes();
    }
    void selecionaItaliano()
    {
        LinguaAtual = "Italiano";
        PAtivo = false;
        InAtivo = false;
        AlAtivo = false;
        EsAtivo = false;
        ItaAtivo = true;
        audioManagerInstance.Play("som_click");
        Troca_Lingua();
        AtualizaSessoes();
    }
    void AtualizaSessoes()
    {
        Atualiza_sessao_Frases_Expressoes();
    }

     void Escolhe_sessao_Frases_Expressoes()
    {
        SessaoAtual = "FrasesEexpressoes";
        Cria_Titulo_Frase_Expressao(LinguaAtual);
        var portview = Resources
            .FindObjectsOfTypeAll<GameObject>()
            .FirstOrDefault(g => g.CompareTag("rolagem"));
        portview.GetComponent<RectTransform>().sizeDelta = new Vector2(341, -4069);
        portview.GetComponent<RectTransform>().position = new Vector2(550, -1731);
        using ( var connection = new SqliteConnection(dbName))
        {
            connection.Open();
            using (var command = connection.CreateCommand())
            {
                string sqlcmd = "SELECT * FROM Palavras WHERE Sessao == '"+SessaoAtual+"'";
                command.CommandText = sqlcmd;
                using (IDataReader reader = command.ExecuteReader())
                {
                    if(LinguaAtual == "Portugues")
                    {
                        while (reader.Read())
                        {
                            Cria_Card_reduzidos_FrasesEExpressoes(System.Convert.ToInt32(reader["id"]),reader["Portugues"].ToString());
                            Debug.Log(reader["id"].ToString() + reader["Portugues"].ToString() + reader["Ingles"].ToString());

                        }
                        // Preciso chamar isso para não duplicar as vezes que a sessão for criada
                        FrasesEExpressosCriado = true;
                    }
                    else if (LinguaAtual == "Ingles")
                    {
                        while (reader.Read())
                        {
                            
                            Cria_Card_reduzidos_FrasesEExpressoes(System.Convert.ToInt32(reader["id"]), reader["Ingles"].ToString());
                            //Debug.Log(reader["Ingles"]);

                        }
                        // Preciso chamar isso para não duplicar as vezes que a sessão for criada
                        FrasesEExpressosCriado = true;
                    }
                    else if (LinguaAtual == "Alemao")
                    {
                        while (reader.Read())
                        {
                            
                            Cria_Card_reduzidos_FrasesEExpressoes(System.Convert.ToInt32(reader["id"]), reader["Alemao"].ToString());
                            //Debug.Log(reader["Alemao"]);

                        }
                        // Preciso chamar isso para não duplicar as vezes que a sessão for criada
                        FrasesEExpressosCriado = true;
                    }
                    else if (LinguaAtual == "Espanhol")
                    {
                        while (reader.Read())
                        {
                            
                            Cria_Card_reduzidos_FrasesEExpressoes(System.Convert.ToInt32(reader["id"]), reader["Espanhol"].ToString());
                            //Debug.Log(reader["Espanhol"]);

                        }
                        // Preciso chamar isso para não duplicar as vezes que a sessão for criada
                        FrasesEExpressosCriado = true;
                    }
                    else if (LinguaAtual == "Italiano")
                    {
                        while (reader.Read())
                        {
                            
                            Cria_Card_reduzidos_FrasesEExpressoes(System.Convert.ToInt32(reader["id"]), reader["Italiano"].ToString());
                            //Debug.Log(reader["Italiano"]);

                        }
                        // Preciso chamar isso para não duplicar as vezes que a sessão for criada
                        FrasesEExpressosCriado = true;
                    }
                    

                }
            }
            connection.Clone();
        }
    }
    void Atualiza_sessao_Frases_Expressoes()
    {

        var obj = Resources
        .FindObjectsOfTypeAll<GameObject>()
        .FirstOrDefault(g => g.CompareTag("rolagem"));
        GameObject[] AllObjects = GameObject.FindGameObjectsWithTag("FraseExpressao");
        string[] Novatraducao = new string[49];
        
        using (var connection = new SqliteConnection(dbName))
        {
            connection.Open();
            using (var command = connection.CreateCommand())
            {
                string sqlcmd = "SELECT * FROM Palavras WHERE Sessao == '" + SessaoAtual + "'";
                command.CommandText = sqlcmd.ToString();
                using (IDataReader reader = command.ExecuteReader())
                {
                    if (LinguaAtual == "Portugues")
                    {
                        while (reader.Read())
                        {
                            int inde = System.Convert.ToInt32(reader["id"]) - 1;
                            Novatraducao[inde] = reader["Portugues"].ToString();
                            for (int i = 0; i < AllObjects.Length; i++)
                            {
                                AllObjects[i].GetComponentInChildren<Text>().text = Novatraducao[i];
                            }
                            //Debug.Log(reader["Portugues"]);

                        }
                        // Preciso chamar isso para não duplicar as vezes que a sessão for criada
                        FrasesEExpressosCriado = true;
                    }
                    else if (LinguaAtual == "Ingles")
                    {
                        while (reader.Read())
                        {
                            try
                            {
                                int inde = System.Convert.ToInt32(reader["id"]) - 1;
                                Novatraducao[inde] = reader["Ingles"].ToString();
                                for (int i = 0; i < AllObjects.Length; i++)
                                {
                                    AllObjects[i].GetComponentInChildren<Text>().text = Novatraducao[i];
                                }
                            }
                            catch
                            {
                                Debug.Log(Novatraducao);
                            }
                            
                            //Debug.Log(reader["Ingles"]);

                        }
                        // Preciso chamar isso para não duplicar as vezes que a sessão for criada
                        FrasesEExpressosCriado = true;
                    }
                    else if (LinguaAtual == "Alemao")
                    {
                        while (reader.Read())
                        {
                            int inde = System.Convert.ToInt32(reader["id"]) - 1;
                            Novatraducao[inde] = reader["Alemao"].ToString();
                            for (int i = 0; i < AllObjects.Length; i++)
                            {
                                AllObjects[i].GetComponentInChildren<Text>().text = Novatraducao[i];
                            }

                            //Debug.Log(reader["Alemao"]);

                        }
                        // Preciso chamar isso para não duplicar as vezes que a sessão for criada
                        FrasesEExpressosCriado = true;
                    }
                    else if (LinguaAtual == "Espanhol")
                    {
                        while (reader.Read())
                        {

                            int inde = System.Convert.ToInt32(reader["id"]) - 1;
                            Novatraducao[inde] = reader["Espanhol"].ToString();
                            for (int i = 0; i < AllObjects.Length; i++)
                            {
                                AllObjects[i].GetComponentInChildren<Text>().text = Novatraducao[i];
                            }
                            //Debug.Log(reader["Espanhol"]);

                        }
                        // Preciso chamar isso para não duplicar as vezes que a sessão for criada
                        FrasesEExpressosCriado = true;
                    }
                    else if (LinguaAtual == "Italiano")
                    {
                        while (reader.Read())
                        {
                            int inde = System.Convert.ToInt32(reader["id"]) - 1;
                            Novatraducao[inde] = reader["Italiano"].ToString();
                            for (int i = 0; i < AllObjects.Length; i++)
                            {
                                AllObjects[i].GetComponentInChildren<Text>().text = Novatraducao[i];
                            }

                        }
                        
                        
                    }


                }
            }
            connection.Clone();
        }
        
    }

    void Cria_Card_reduzidos_FrasesEExpressoes(int idFoto,string Nome)
    {
        if (!FrasesEExpressosCriado)
        {
            GameObject card_reduzido = Instantiate(Baseprefab, inicialpositionSec + offset, Quaternion.identity) as GameObject;
            var obj = Resources
        .FindObjectsOfTypeAll<GameObject>()
        .FirstOrDefault(g => g.CompareTag("rolagem"));
            card_reduzido.transform.SetParent(obj.transform, false);
            card_reduzido.tag = "FraseExpressao";

            var objTitulo = Resources
        .FindObjectsOfTypeAll<GameObject>()
        .FirstOrDefault(g => g.CompareTag("Titulo_Base"));
            objTitulo.GetComponent<Text>().text = Nome;
            var objImg = Resources
        .FindObjectsOfTypeAll<GameObject>()
        .FirstOrDefault(g => g.CompareTag("Image_Base"));

            objImg.GetComponent<Image>().sprite = AssetsFraseseExpressoes[idFoto - 1];


            offset -= new Vector3(0, 80, 0);
        }
        else
        {
            Debug.Log("Sessão Já criada");
        }

        
        

    }
    void Cria_Titulo_Frase_Expressao(string lingua)
        /*
         * Essa Função não vai ter a Tradução pois seu nome é muito grande e nomes compostos não estão sendo quebrados!.
         */
    {
        var obj = Resources
    .FindObjectsOfTypeAll<GameObject>()
    .FirstOrDefault(g => g.CompareTag("Cartoes"));

        GameObject card_titulo = Instantiate(TituloSessoes, inicialposition, Quaternion.identity, obj.transform);

        var objTitulo = Resources
    .FindObjectsOfTypeAll<GameObject>()
    .FirstOrDefault(g => g.CompareTag("Titulo"));
        /*
         * Tem que separar um por um dos objetos mesmo que estejam desativados. confia Felipe
         */
        //objTitulo.GetComponentInChildren<Image>().sprite = Titulo_Frases_Expressoes.sprite;
        var objTituloImg = Resources
    .FindObjectsOfTypeAll<GameObject>()
    .FirstOrDefault(g => g.CompareTag("imgTitulo"));
        objTituloImg.GetComponent<Image>().sprite = Titulo_Frases_Expressoes.sprite;
        
        if (lingua == "Portugues")
        {
            objTitulo.GetComponentInChildren<Text>().text = @"Frases
Expressões";
        }
        else if (lingua == "Ingles")
        {
            objTitulo.GetComponentInChildren<Text>().text = @"Phrases
Expressions";
        }
        else if (lingua == "Alemao")
        {
            objTitulo.GetComponentInChildren<Text>().text = @"Sätze
Ausdrücke";
        }
        else if (lingua == "Espanhol")
        {
            objTitulo.GetComponentInChildren<Text>().text = @"Frases
Expresiones";
        }
        else if (lingua == "Italiano")
        {
            objTitulo.GetComponentInChildren<Text>().text = @"Frasi
Espressioni";
        }

    }
    IEnumerator PequenaEspera()
    {
        yield return new WaitForSeconds(0.2f);
    }

}
