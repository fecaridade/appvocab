﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class PlayfabManager : MonoBehaviour
{
    
    public InputField emailInput_TelaLogin;
    public InputField passwordInput_TelaLogin;
    public Button loginButton_TelaLogin;
    public Button cadastre_seButton;
    public Button LoginWithGoogle;
    public Button LoginWithFacebook;
    public Button EntrarButton;
    // Register datasaving
    public InputField username;
    public InputField emailInput_TelaCadastre_se;
    public InputField passwordInput_TelaCadastre_se;
    public InputField confirmpasswordInput_TelaCadastre_se;
    public InputField aniversario;
    //Verify if inputsAre corrects
    public bool diffpasswords;
    // CanvasManager Instance
    CanvasManager canvasManager;
    //for google play debug
    public Text debug;

    public GetPlayerCombinedInfoRequest InfoRequest;


    void Start()
    {
        initialize();
        canvasManager = CanvasManager.GetInstance();
        cadastre_seButton.onClick.AddListener(RegisterButton);
        EntrarButton.onClick.AddListener(LoginButton);
        LoginWithGoogle.onClick.AddListener(OnLoginWithGoogleClicked);
        debug = GameObject.Find("Debug").GetComponent<Text>();
        GooglePlayGames.PlayGamesPlatform.Activate();
    }
    void initialize()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
            .RequestServerAuthCode(false)
            .Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
        debug.text = "play games initialized";
    }
    public void RegisterButton()
    {
        var request = new RegisterPlayFabUserRequest
        {
            Email = emailInput_TelaCadastre_se.text,
            Password = passwordInput_TelaCadastre_se.text,
            RequireBothUsernameAndEmail = false
        };
        PlayFabClientAPI.RegisterPlayFabUser(request, OnRegisterSucesseful, OnError);
    }

    public void SaveBasicData()
    {
        
        var request = new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>
            {
                {"username",username.text },
                { "aniversario",aniversario.text}
            }
        };
        PlayFabClientAPI.UpdateUserData(request, OnDataSend, OnError);
    }
    public void OnDataSend(UpdateUserDataResult result)
    {
        print("Sucesso a o enviar dados");
    }
    public void LoginButton()
    {
        var request = new LoginWithEmailAddressRequest
        {
            Email = emailInput_TelaLogin.text,
            Password = passwordInput_TelaLogin.text,
        };
        PlayFabClientAPI.LoginWithEmailAddress(request, OnLoginSucesseful, OnError);
    }


    void OnLoginSucesseful(LoginResult result)
    {
        Debug.Log("LoggedIn");
        StartCoroutine(pausa());
        Loader.Load(Loader.Scene.Home);

    }
    void OnRegisterSucesseful(RegisterPlayFabUserResult result)
    {
        SaveBasicData();
        Debug.Log("Registed and LoggedIn");
        canvasManager.SwitchCanvas(CanvasType.BemvindoTela);

    }

    IEnumerator pausa()
    {
        yield return new WaitForSeconds(2);
    }

    void OnError(PlayFabError error)
    {
        print(error.ToString());
        Debug.Log("error while logggin in create account!");
        Debug.Log(error.GenerateErrorReport());
    }

    public void paraTelaDeCadastro()
    {
        Loader.Load(Loader.Scene.Registrar);
    }
    public void VoltarDaTelaRegistro()
    {
        Loader.Load(Loader.Scene.LoginCadastrese);
    }
    void OnLoginWithGoogleClicked()
    {
        if (Social.localUser.authenticated)
        {
            debug.text = "Bem vindo";
            canvasManager.SwitchCanvas(CanvasType.SOsVerbos);

        }
        else
        {
            PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptOnce, (success) =>
            {
                switch (success)
                {
                    case SignInStatus.Success:
                        debug.text = "Signed";
                        canvasManager.SwitchCanvas(CanvasType.BemvindoTela);
                        break;

                    default:
                        debug.text = "Signed failed";
                        break;
                }
            });

        }
    }
}
