﻿using UnityEngine;
using UnityEngine.UI;

public enum ButtonType
{
    LOGIN_WITH_EMAIL,
    CADASTRE_SE,
    ESQUECEU_SENHA,
    BEMVINDO_TELA,
    HOME_TELA,
    CARTAS_TELA,
    SUBTELAS,
    SAIBACOMOTELA,
    SUBTELASANIMAIS,
    SUBTELASCORES,
    SUBTELASFAMILIA,
    SUBTELASPRONOME,
    SUBTELASPROFISSOES,
    SUBTELASROUPAS,
    SUBTELASFRUTAS,
    SUBTELASVEGETAIS,
    SUBTELASCAFEDMANHA,
    SUBTELASALMOCO_JANTA,
    SUBTELASVERBOS,
    SUBTELASNUMEROS,
    SUBTELASORGAOS,
    SUBTELASCOZINHA,
    SUBTELAESCRITORIO,
    SUBTELAS5SENTIDOS,
    SUBTELASCONECTIVOS,
    SUBTELASNATUREZA,
    SUBTELASCORPO,
    SUBTELASFRASESEEXPRESSOES,
    SUBTELASOSCONTINENTESEPAISES,
    SUBTELASESPORTES,
    SUBTELASOSLUGARES,
    SUBTELASBANHEIRO,
    SUBTELASSALADEESTAR,
    SUBTELASESCRITORIOS,
    SUBTELASADJETIVOS,
    SUBTELASACESSORIOS,
    SUBTELASMEIOSDETRANSPORTE,
    SUBTELASDIRECOES,
    SUBTELASTEMPO,
    SUBTELASEMOCOESESENTIMENTOS,
    SUBCCOMODOS,
    SUBCAOCOMODOS,
    SUBTELASQUARTOS,
    TELA_PERFIL,
    TELA_CARTAO
}

[RequireComponent(typeof(Button))]
public class ButtonController : MonoBehaviour
{
    public ButtonType buttonType;

    CanvasManager canvasManager;
    Button menuButton;

    private void Start()
    {
        menuButton = GetComponent<Button>();
        menuButton.onClick.AddListener(OnButtonClicked);
        canvasManager = GetComponent<CanvasManager>();
        canvasManager = CanvasManager.GetInstance();
    }

    void OnButtonClicked()
    {
        switch (buttonType)
        {
            case ButtonType.LOGIN_WITH_EMAIL:
                //Call other code that is necessary to start the game like levelManager.StartGame()
                canvasManager.SwitchCanvas(CanvasType.LoginComEmailTela);
                break;
            case ButtonType.CADASTRE_SE:
                //Do More Things like SaveSystem.Save()
                canvasManager.SwitchCanvas(CanvasType.CadastrarTela);
                break;
            case ButtonType.BEMVINDO_TELA:
                canvasManager.SwitchCanvas(CanvasType.BemvindoTela);
                break;
            case ButtonType.SAIBACOMOTELA:
                canvasManager.SwitchCanvas(CanvasType.SaibaComoTela);
                break;
            case ButtonType.HOME_TELA:
                canvasManager.SwitchCanvas(CanvasType.HomeTela);
                break;
            case ButtonType.CARTAS_TELA:
                canvasManager.SwitchCanvas(CanvasType.CartasTela);
                break;
            case ButtonType.SUBTELASANIMAIS:
                canvasManager.SwitchCanvas(CanvasType.SAnimais);
                break;
            case ButtonType.SUBTELASCORES:
                canvasManager.SwitchCanvas(CanvasType.SCores);
                break;
            case ButtonType.SUBTELASFAMILIA:
                canvasManager.SwitchCanvas(CanvasType.SFamilia);
                break;
            case ButtonType.SUBTELASPRONOME:
                canvasManager.SwitchCanvas(CanvasType.SPronome);
                break;
            case ButtonType.SUBTELASPROFISSOES:
                canvasManager.SwitchCanvas(CanvasType.SProfissoes);
                break;
            case ButtonType.SUBTELASROUPAS:
                canvasManager.SwitchCanvas(CanvasType.SRoupas);
                break;
            case ButtonType.SUBTELASFRUTAS:
                canvasManager.SwitchCanvas(CanvasType.SFrutas);
                break;
            case ButtonType.SUBTELASVEGETAIS:
                canvasManager.SwitchCanvas(CanvasType.SVegetais);
                break;
            case ButtonType.SUBTELASCAFEDMANHA:
                canvasManager.SwitchCanvas(CanvasType.SCafedaManha);
                break;
            case ButtonType.SUBTELASALMOCO_JANTA:
                canvasManager.SwitchCanvas(CanvasType.SAlmocoJanta);
                break;
            case ButtonType.SUBTELASVERBOS:
                canvasManager.SwitchCanvas(CanvasType.SOsVerbos);
                break;
            case ButtonType.SUBTELAS5SENTIDOS:
                canvasManager.SwitchCanvas(CanvasType.S5SentidosOutros);
                break;
            case ButtonType.SUBTELASNUMEROS:
                canvasManager.SwitchCanvas(CanvasType.SOsNumeros);
                break;
            case ButtonType.SUBTELASORGAOS:
                canvasManager.SwitchCanvas(CanvasType.SOrgaos);
                break;
            case ButtonType.SUBTELASNATUREZA:
                canvasManager.SwitchCanvas(CanvasType.SANatureza);
                break;
            case ButtonType.SUBTELASCONECTIVOS:
                canvasManager.SwitchCanvas(CanvasType.SConectivos);
                break;
            case ButtonType.SUBTELASCORPO:
                canvasManager.SwitchCanvas(CanvasType.SOCorpo);
                break;
            case ButtonType.SUBTELASFRASESEEXPRESSOES:
                canvasManager.SwitchCanvas(CanvasType.SAsFrasesEExpressoes);
                break;
            case ButtonType.SUBTELASOSCONTINENTESEPAISES:
                canvasManager.SwitchCanvas(CanvasType.SOsContinentesEOsPaíses);
                break;
            case ButtonType.SUBTELASESPORTES:
                canvasManager.SwitchCanvas(CanvasType.SEsportes);
                break;
            case ButtonType.SUBTELASOSLUGARES:
                canvasManager.SwitchCanvas(CanvasType.SOsLugares);
                break;
            case ButtonType.SUBTELASADJETIVOS:
                canvasManager.SwitchCanvas(CanvasType.SAdjetivos);
                break;
            case ButtonType.SUBTELASACESSORIOS:
                canvasManager.SwitchCanvas(CanvasType.SAcessorios);
                break;            
            case ButtonType.SUBTELASMEIOSDETRANSPORTE:
                canvasManager.SwitchCanvas(CanvasType.SMeiosdeTransporte);
                break;
            case ButtonType.SUBTELASDIRECOES:
                canvasManager.SwitchCanvas(CanvasType.SDirecoes);
                break;
            case ButtonType.SUBTELASTEMPO:
                canvasManager.SwitchCanvas(CanvasType.STempo);
                break;
            case ButtonType.SUBCCOMODOS:
                canvasManager.SwitchCanvas(CanvasType.SSComodos);
                break;
            case ButtonType.SUBTELASCOZINHA:
                canvasManager.SwitchCanvas(CanvasType.SCozinha);
                break;
            case ButtonType.SUBTELASESCRITORIOS:
                canvasManager.SwitchCanvas(CanvasType.SEscritorio);
                break;
            case ButtonType.SUBTELASSALADEESTAR:
                canvasManager.SwitchCanvas(CanvasType.SSalaDeEstar);
                break;
            case ButtonType.SUBTELASQUARTOS:
                canvasManager.SwitchCanvas(CanvasType.SQuarto);
                break;
            case ButtonType.SUBTELASBANHEIRO:
                canvasManager.SwitchCanvas(CanvasType.SBanheiro);
                break;
            case ButtonType.SUBTELASEMOCOESESENTIMENTOS:
                canvasManager.SwitchCanvas(CanvasType.SAsEmoçoesESentimentos);
                break;
            case ButtonType.TELA_PERFIL:
                canvasManager.SwitchCanvas(CanvasType.Perfil);
                break;
            case ButtonType.TELA_CARTAO:
                canvasManager.SwitchCanvas(CanvasType.Cartao);
                break;
            default:
                break;
        }
    }
}