﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScrolLimit : MonoBehaviour
{
    public ScrollRect scrollRect;
    public Rect seccoes;
    public float limiteParaBaixo;
    public float limiteParaCima;
    public bool debug;
    void Start()
    {
        
    }

    // Update is called once per frame
    
    void LateUpdate()
    {
        LimitScroll();
    }
    void LimitScroll()
    {
        if (debug)
        {
            print(scrollRect.content.localPosition.y);
        }
        if (scrollRect.content.localPosition.y >= limiteParaBaixo)
        {
            scrollRect.content.localPosition = new Vector3(scrollRect.content.localPosition.x, limiteParaBaixo, 0);
        }
        else if(scrollRect.content.localPosition.y <= limiteParaCima)
        {
            scrollRect.content.localPosition = new Vector3(scrollRect.content.localPosition.x, limiteParaCima, 0);
        }
    }
}
