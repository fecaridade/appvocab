﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Data;
public enum CanvasType
{
    MenuLoginTela,
    LoginComEmailTela,
    CadastrarTela,
    BemvindoTela,
    SaibaComoTela,
    HomeTela,
    CartasTela,
    SAnimais,
    SCores,
    SFamilia,
    SPronome,
    SProfissoes,
    SRoupas,
    SFrutas,
    SVegetais,
    SCafedaManha,
    SAlmocoJanta,
    SOsVerbos,
    SAsEmoçoesESentimentos,
    SOsNumeros,
    SOCorpo,
    SOrgaos,
    S5SentidosOutros,
    SConectivos,
    SANatureza,
    SAsFrasesEExpressoes,
    SOsContinentesEOsPaíses,
    SEsportes,
    SOsLugares,
    SAdjetivos,
    SAcessorios,
    SEscritorio,
    SComodos,
    SMeiosdeTransporte,
    SDirecoes,
    STempo,
    SCozinha,
    SBanheiro,
    SQuarto,
    SSalaDeEstar,
    SSComodos,
    Perfil,
    Cartao,



}

public class CanvasManager : Singleton<CanvasManager>
{
    List<CanvasController> canvasControllerList;
    CanvasController lastActiveCanvas;
    
    
    void FixedUpdate()
    {
        
    }
    void start()
    {

    }

    void Awake()
    {
        canvasControllerList = GetComponentsInChildren<CanvasController>().ToList();
        canvasControllerList.ForEach(x => x.gameObject.SetActive(false));
        
        if (SceneManager.GetSceneByName("Home").name == "Home")
        {
            SwitchCanvas(CanvasType.CartasTela);


        }
        else if (SceneManager.GetSceneByName("Login").name == "Login")
        {
            SwitchCanvas(CanvasType.MenuLoginTela);

        }
        
    }


    public void SwitchCanvas(CanvasType _type)
    {
        if (lastActiveCanvas != null)
        {
            lastActiveCanvas.gameObject.SetActive(false);
        }

        CanvasController desiredCanvas = canvasControllerList.Find(x => x.canvasType == _type);
        if (desiredCanvas != null)
        {
            desiredCanvas.gameObject.SetActive(true);
            lastActiveCanvas = desiredCanvas;
        }
        else { Debug.LogWarning("The desired canvas was not found!"); }
    }
    

}

