﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class GameControl : MonoBehaviour
{

    [SerializeField] public RectTransform palavra1;
    [SerializeField] public RectTransform palavra2;
    [SerializeField] public RectTransform imagem;
    public float offesetP1 = 0.5f;
    public float offesetP2 = 0.5f;
    private void Start()
    {

    }

    private void Update()
    {


        if (imagem.anchoredPosition.y >= palavra1.anchoredPosition.y - offesetP1)
        {
            Debug.Log("Escolheu palavra 1");
        }
        if (imagem.anchoredPosition.y <= palavra2.anchoredPosition.y + offesetP2)
        {
            Debug.Log("Escolheu palavra 2");
        }
    }
}
