﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ImageSpawn : MonoBehaviour
{
    int rand;
    public Sprite[] ImageSprite;
    void Start()
    {
        ChangeImage();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void ChangeImage()
    {
        rand = Random.Range(0, ImageSprite.Length);
        GetComponent<Image>().sprite = ImageSprite[rand];
        Debug.Log("Mudou a imagem" + rand);
    }
}
