﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SaibaComoTela : MonoBehaviour
{
    public Button continuarButton;
    void Start()
    {
        continuarButton.onClick.AddListener(loadHomeScene);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void loadHomeScene()
    {

        Loader.Load(Loader.Scene.Home);
    }
    IEnumerator pausa()
    {
        yield return new WaitForSeconds(2);
    }
}
